$(document).ready(function () {
    $('#page-one-button').click(function () { 
        $('#page-one').hide();
        $(this).hide();
        $('#second-circle').show();
        $('#page-two').show();

    });

    $('#page-two-button').click(function () { 
        $('#page-two').hide();
        $('#page-three').show();
        $('#third-circle').show();
        $('#page-three-button').show();

    });

    $('#add-coupon').click( function () {
        $('.subscription__forms').show();
    });

    $('#coupon').keyup( function() {
        if ($(this).val()) {
            $('.apply-button').removeAttr('disabled');
            $('#helpId').css('opacity', '0');
        }
        else {
            $('.apply-button').attr('disabled', 'disabled');
            $('#helpId').css('opacity', '1');
        }
    });

    $('#new-button').click( function () {
        $('#all-forms').show();
        $(this).attr('disabled', 'disabled');
    });

    $('.verification').keyup( function () {
        var isValid = true;
        $('.verification').each(function() {
           if(!$(this).val()) {
               isValid = false;
           }
       })
        if(isValid) {
            $('.billings__input-container--hidden').show();
        } 
        else {
             $('.billings__input-container--hidden').hide();
        }
    });

    $('#title-input').keyup( function() {
        if ($(this).val()) {
            $('#billings__save').removeAttr('disabled');
        } 
        else {
            $('#billings__save').attr('disabled', 'disabled');
        }
    });

    $('#billings__save').click( function(e) {
        e.preventDefault();
        $('#billings__edit').show();
        $('#billings__delete').show();
        $(this).hide();
        
    });
    
    $('#monthly').click(function() {
       $('#annual .subscription__hover-touched').hide();
       $('#annual').removeClass('selected');
       $(this).addClass('selected');
       $('#monthly .subscription__hover-touched').show();
       $('.billing-amount').html('$39');
    })

    $('#annual').click(function() {
       $('#monthly .subscription__hover-touched').hide();
       $('#monthly').removeClass('selected');
       $(this).addClass('selected');
       $('#annual .subscription__hover-touched').show();
       $('.billing-amount').html('$99');
    })
    
});